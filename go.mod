module github.com/jauderho/bl3auto

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/shibukawa/configdir v0.0.0-20170330084843-e180dbdc8da0
	github.com/thedevsaddam/gojsonq v2.3.0+incompatible
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
)
